package com.example.dav.myapplication_gps;

import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;


import com.github.mikephil.charting.charts.LineChart;


public class MainActivity3Activity extends ActionBarActivity {
    private RelativeLayout mainLayout;
    private LineChart mChart;

    List<int[]> initial;
    int[] my_year;
    int[] my_units_sold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity3);
        mainLayout=(RelativeLayout) findViewById(R.id.mainLayout);
        mChart=new LineChart(this);
        mainLayout.addView(mChart);
        mChart.setDescription("");
        mChart.setNoDataTextDescription("No data for the moment");
        mChart.setHighlightEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        mChart.setPinchZoom(true);

        mChart.setBackgroundColor(Color.LTGRAY);


        LineData data= new LineData();
        data.setValueTextColor(Color.WHITE);
        mChart.setData(data);
        Legend l= mChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.WHITE);

        XAxis x1= mChart.getXAxis();
        x1.setTextColor(Color.WHITE);
        x1.setDrawGridLines(false);
        x1.setAvoidFirstLastClipping(true);


        YAxis y1= mChart.getAxisLeft();
        y1.setTextColor(Color.WHITE);
        y1.setAxisMaxValue(120f);
        y1.setDrawGridLines(true);

        YAxis y12= mChart.getAxisRight();
        y12.setEnabled(false);

        posgpsBDD posgpsBDD= new posgpsBDD(this);
        posgpsBDD.open();
        String lng = posgpsBDD.showAll();


    }


    protected void onResume(){
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<10; i++){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addEntry();
                        }
                    });
                    try{
                        Thread.sleep(600);
                    } catch (InterruptedException e){
                        //manage error
                    }
                }
            }
        }).start();
    }

    private void addEntry(){
        LineData data= mChart.getData();

        if (data!= null) {
            LineDataSet set= data.getDataSetByIndex(0);
            if (set == null) {
                set= createSet();
                data.addDataSet(set);
            }
            data.addXValue("");
            posgpsBDD posgpsBDD= new posgpsBDD(this);

           float c1=Float.parseFloat(posgpsBDD.showAll());

            //data.addEntry(new Entry((float)(Math.random() * 100)+ 5f, set.getEntryCount()), 0);
            data.addEntry(new Entry(c1,set.getEntryCount()), 0);
            mChart.notifyDataSetChanged();
            mChart.setVisibleXRange(6);
            mChart.moveViewToX(data.getXValCount() -7);


        }}
    private LineDataSet createSet(){
        LineDataSet set= new LineDataSet(null, "DB test");
        set.setDrawCubic(true);
        // set.setCubicIntensity(0,2f);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(ColorTemplate.getHoloBlue());
        set.setLineWidth(2f);
        set.setCircleSize(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244,117,177));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(10f);

        return set;



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
