package com.example.dav.myapplication_gps;

import android.app.Dialog;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.NumberPicker;
//import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.content.Context;
import android.widget.Toast;
import android.widget.ImageView;

import java.util.Locale;


public class MainActivity2Activity extends ActionBarActivity implements OnClickListener, NumberPicker.OnValueChangeListener{
    NumberPicker np;
    final String EXTRA_Nom = "user_nom";
    private posgpsBDD posgpsBDD ;
    posgpsBDD db = new posgpsBDD(this);
    private EditText tv;
    private EditText tv1;
    private EditText tv2;
    private RadioGroup radioGroup;
    private RadioButton radioButton, radioButton2;
    ImageView imageView;
    Uri imageUri = Uri.parse("android.resource://com.example.dav.myapplication/drawable/no_user_logo.png");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);

        Intent intent = getIntent();
        //final EditText firstname = (EditText) findViewById(R.id.editText1);
        //final EditText username = (EditText) findViewById(R.id.editText1);
       //final String s1 = login.getText().toString();

       // np = (NumberPicker) findViewById(R.id.numberPicker1);
//        np.setMinValue(5);
  //      np.setMaxValue(100);
    //    np.setWrapSelectorWheel(true);
       // final Button button = (Button) findViewById(R.id.buton_save);
        tv= (EditText) findViewById(R.id.editText3);
        tv1= (EditText) findViewById(R.id.editText4);
        tv2= (EditText) findViewById(R.id.editText5);
        imageView = (ImageView) findViewById(R.id.imageView);
      //  Button b= (Button) findViewById(R.id.picker1);
        findViewById(R.id.buton_save).setOnClickListener(this);
        findViewById(R.id.editText3).setOnClickListener(this);
        findViewById(R.id.editText4).setOnClickListener(this);
        findViewById(R.id.editText5).setOnClickListener(this);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Contact Image"), 1);
            }

        });
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
           public void onCheckedChanged(RadioGroup group, int checkedId) {}

             /*   // find which radio button is selected

                if(checkedId == R.id.radioButton) {

                    Toast.makeText(getApplicationContext(), "choice: Male",

                           Toast.LENGTH_SHORT).show();

                } else  {

                    //Toast.makeText(getApplicationContext(), "choice: Female",
                          Toast.LENGTH_SHORT).show();}*/



    });}


        public void onClick(View v) {
            // test trannsfert activity 2 vers 1
            //Intent intent = new Intent(MainActivity2Activity.this, MainActivity.class);
            //intent.putExtra(EXTRA_Nom, login.getText().toString());
            //startActivity(intent);
                switch (v.getId()) {
                    case R.id.buton_save:
                        sauvegarderposition();
                        break;
                    case R.id.editText3:
                        afficherpicker1();
                        break;
                    case R.id.editText4:
                        afficherpicker2();
                        break;
                    case R.id.editText5:
                        afficherpicker3();
                        break;
                    default:
                        break;





                }


    }








public void sauvegarderposition(){

       TextView firstname = (EditText) findViewById(R.id.editText1);
        TextView username = (EditText) findViewById(R.id.editText2);
    TextView age = (EditText) findViewById(R.id.editText3);
    TextView weight = (EditText) findViewById(R.id.editText4);
    TextView taille = (EditText) findViewById(R.id.editText5);


        String s1 = firstname.getText().toString();
        String s2 = username.getText().toString();
    String s3 = age.getText().toString();
    String s4 = weight.getText().toString();
    String s5 = taille.getText().toString();

    if(s1.trim().length()==0|| s2.trim().length()==0 ||s3.trim().length()==0||s4.trim().length()==0||s5.trim().length()==0 )
    {
      Toast.makeText(this," Veuillez compléter l'ensemble des champs",Toast.LENGTH_LONG).show();
        return;
    }
    int selectedId = radioGroup.getCheckedRadioButtonId();
    boolean isAllowUpdate = false;
    switch(selectedId) {
        case R.id.radioButton : isAllowUpdate = true; break;
        case R.id.radioButton2 : isAllowUpdate = false; break;
    }

//save it to database
    if(isAllowUpdate){
        Integer t1=1;

    }
    // true ==> save 1 value
    else{
    // false
        Integer t1=0;
    }}


/*

        posgpsBDD posgpsBDD= new posgpsBDD(this);
        usergps usergps=new usergps(s1,s2,s3,s4,s5);

        posgpsBDD.open();
        posgpsBDD.insertusergps(usergps);
       // ((TextView) findViewById(R.id.editText2)).setText(String.valueOf(usergps));
    Toast.makeText(this, usergps.toString(), Toast.LENGTH_LONG).show();
        posgpsBDD.close();
*/




    public void afficherpicker1(){
      final Dialog d = new Dialog(MainActivity2Activity.this);
      d.setTitle("Votre âge");
      d.setContentView(R.layout.dialog);
      Button b1 = (Button) d.findViewById(R.id.button1);
      Button b2 = (Button) d.findViewById(R.id.button2);
      final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
      np.setValue(25);
      np.setMaxValue(110); // max value 100
      np.setMinValue(12);   // min value 0
      np.setWrapSelectorWheel(false);
      np.setOnValueChangedListener(this);
      b1.setOnClickListener(new OnClickListener()
      {
          @Override
          public void onClick(View v) {
              tv.setText(String.valueOf(np.getValue())); //set the value to textview
              d.dismiss();
          }
      });


      b2.setOnClickListener(new OnClickListener(){
          @Override
          public void onClick(View v) {
              d.dismiss(); // dismiss the dialog
          }
      });
      d.show();



  }
    public void afficherpicker2(){
        final Dialog d = new Dialog(MainActivity2Activity.this);
        d.setTitle("Votre poids (en Kg)");
        d.setContentView(R.layout.dialog);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setValue(70);
        np.setMaxValue(130); // max value 100
        np.setMinValue(50);   // min value 0
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv1.setText(String.valueOf(np.getValue())); //set the value to textview
                d.dismiss();
            }
        });


        b2.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();


    }
    public void afficherpicker3(){
        final Dialog d = new Dialog(MainActivity2Activity.this);
        d.setTitle("Votre taille (en cm)");
        d.setContentView(R.layout.dialog);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setValue(175);
        np.setMaxValue(220); // max value 100
        np.setMinValue(130);   // min value 0
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b1.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv2.setText(String.valueOf(np.getValue())); //set the value to textview
                d.dismiss();
            }
        });


        b2.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();


    }
   public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == RESULT_OK) {
               if (reqCode == 1) {


                imageView.setImageURI(data.getData());
            }
        }
    }











    @Override



    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }
@Override

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
}
