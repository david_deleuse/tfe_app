package com.example.dav.myapplication_gps;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.Menu;
import android.view.MenuItem;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.database.Cursor;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
//date android
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import  android.text.format.Time;
import java.util.Random;
import java.util.TimerTask;

import javax.xml.transform.Result;

//test ifonction pdf
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
//import com.itextpdf.text.List;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Header;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



//OnClickKistener, LocationListener
public class MainActivity extends ActionBarActivity implements OnClickListener, LocationListener {
    private LocationManager lManager;
    private Location location;
    private String choix_source = "";
    private EditText input;
    private posgpsBDD posgpsBDD ;
    private ProgressBar bar;
   // private MaBaseSQLite maBaseSQLite;
  //  private SQLiteDatabase bdd;
   posgpsBDD db = new posgpsBDD(this);
    boolean isGPSEnabled = false;
    final String EXTRA_Nom = "user_nom";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bar = (ProgressBar) findViewById(R.id.progressBar1);
        Button button = (Button) findViewById(R.id.button_stop);
        button.setBackgroundColor(0xfff00000);
        Button button2 = (Button) findViewById(R.id.obtenir_position);
        button2.setBackgroundColor(0xff00ff00);
        Boolean isFirstRun= getSharedPreferences("PREFERENCE", MODE_PRIVATE ).getBoolean("isfirstrun",true);
        if (isFirstRun){
            startActivity(new Intent(MainActivity.this, MainActivity2Activity.class));

            getSharedPreferences("PREFERENCE", MODE_PRIVATE ).edit().putBoolean("isfirstrun",false).commit();

        }
        //posgpsBDD posgpsBDD= new posgpsBDD(this);
        //posgpsBDD.open();
        //posgpsBDD.insertposgpstest();


        //Toast.makeText(this,list,Toast.LENGTH_LONG).show();

        //posgpsBDD posgpsBDD= new posgpsBDD(this);
        //posgpsBDD.open();

        //On récupère le service de localisation
        lManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        //recreer bdd
        //bdd = maBaseSQLite.getWritableDatabase();
        //Initialisation de l'écran
        reinitialisationEcran();
        input = (EditText) findViewById(R.id.latitude);

        //On affecte un écouteur d'évènement aux boutons
        findViewById(R.id.choix_source).setOnClickListener(this);
        findViewById(R.id.obtenir_position).setOnClickListener(this);
        findViewById(R.id.afficherAdresse).setOnClickListener(this);
        findViewById(R.id.sup).setOnClickListener(this);
        findViewById(R.id.nextrec).setOnClickListener(this);
        findViewById(R.id.firstrec).setOnClickListener(this);
        findViewById(R.id.activity2).setOnClickListener(this);
        findViewById(R.id.obtenir_position2).setOnClickListener(this);
        findViewById(R.id.distancegps).setOnClickListener(this);
        findViewById(R.id.button_stop).setOnClickListener(this);
        findViewById(R.id.button_pdf).setOnClickListener(this);
        findViewById(R.id.button_chart).setOnClickListener(this);



        if ( !lManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
        Intent intent = getIntent();
        TextView loginDisplay = (TextView) findViewById(R.id.adresse);


        if (intent != null) {
            loginDisplay.setText(intent.getStringExtra(EXTRA_Nom));

        }



       /* List<posgps> values = posgpsBDD.getAllComments();*/

        // utilisez SimpleCursorAdapter pour afficher les
        // éléments dans une ListView
      /*  ArrayAdapter<posgps> adapter = new ArrayAdapter<posgps>(this,
                android.R.layout.simple_list_item_1, values);
        //setListAdapter(adapter);*/
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Votre GPS semble être désactivé, voulez-vous l'activer ?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClick(View v) {
        //ArrayAdapter<posgps> adapter = (ArrayAdapter<posgps>) getListAdapter();
        posgps posgps= null;

        switch (v.getId()) {
            case R.id.choix_source:
                choisirSource();
                break;
            case R.id.obtenir_position:

                obtenirPosition();

                break;
            case R.id.afficherAdresse:
                afficherAdresse();
                /*String[] comments = new String[] { "Cool", "Very nice", "Hate it" };
                int nextInt = new Random().nextInt(3);
                // enregistrer le nouveau commentaire dans la base de données
                posgps = posgpsBDD.createComment(comments[nextInt]);
                //adapter.add(posgps);*/
                break;
            case R.id.sup:
                supprimerElement();
                break;
            case R.id.nextrec:
                nextrec();
                break;
            case R.id.firstrec:
                firstrec();
                break;
            case R.id.obtenir_position2:
                obtenirPosition2();
                break;
            case R.id.distancegps:
                distance();
                break;
            case R.id.activity2:
                ouvriractivity2();
                break;
            case R.id.button_stop:
                stopper();
                break;
            case R.id.button_pdf:
                createPDF();
                break;
            case R.id.button_chart:
                ouvrirchart();
                break;
            default:
                break;



        }

    }
    public void startProgress(View view) {

        bar.setProgress(0);
        new Thread(new Task()).start();//.start()
    }
   /* private void turnGPSOn(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }*/

   // List<posgps> table_GPS = posgpsBDD.getAllContacts();

    /*for (posgps posgps : table_GPS) {
        String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
        // Writing Contacts to log
        Log.d("Name: ", log);
    }*/
//Essaie Asynctask
  /*  public class GetPositionTask extends AsyncTask<Void, Void, Location> implements LocationListener {

        final long TWO_MINUTES = 2*60*1000;
        private Location location;
        private LocationManager lm;
        Context mContext;
        public GetPositionTask(Context mContext){
            this.mContext = mContext;
        }


        protected void onPreExecute()
        {

            // Configure location manager - I'm using just the network provider in this example
            lm = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
            nearProgress.setVisibility(View.VISIBLE);
        }

        protected Location doInBackground(Void... params)
        {
            // Try to use the last known position
            Location lastLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            // If it's too old, get a new one by location manager
            if (System.currentTimeMillis() - lastLocation.getTime() > TWO_MINUTES)
            {
                while (location == null)
                    try { Thread.sleep(100); } catch (Exception ex) {}

                return location;
            }

            return lastLocation;
        }

        protected void onPostExecute(Location location)
        {
            nearProgress.setVisibility(View.GONE);
            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            lm.removeUpdates(this);

            // HERE USE THE LOCATION
        }



        @Override
        public void onLocationChanged(Location newLocation)
        {
            location = newLocation;
        }

        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }
*/


    private void reinitialisationEcran(){
        ((TextView)findViewById(R.id.latitude)).setText("0.0");
        ((TextView)findViewById(R.id.longitude)).setText("0.0");
        ((TextView)findViewById(R.id.altitude)).setText("0.0");
        ((TextView)findViewById(R.id.adresse)).setText("");

        findViewById(R.id.obtenir_position).setEnabled(false);
        findViewById(R.id.afficherAdresse).setEnabled(false);
    }

    private void choisirSource() {
        reinitialisationEcran();

        //On demande au service la liste des sources disponibles.
        List <String> providers = lManager.getProviders(true);

        final String[] sources = new String[providers.size()];
        int i =0;
        //on stock le nom de ces source dans un tableau de string
        for(String provider : providers)
            sources[i++] = provider;

        //On affiche la liste des sources dans une fenêtre de dialog
        //Pour plus d'infos sur AlertDialog, vous pouvez suivre le guide
        //http://developer.android.com/guide/topics/ui/dialogs.html
        new AlertDialog.Builder(MainActivity.this)
                .setItems(sources, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        findViewById(R.id.obtenir_position).setEnabled(true);
                        //on stock le choix de la source choisi
                        choix_source = sources[which];
                        //on ajoute dans la barre de titre de l'application le nom de la source utilisé
                        setTitle(String.format("%s - %s", getString(R.string.app_name),
                                choix_source));
                    }
                })
                .create().show();
        Log.d("MainAction","afficher localisation");
    }
    private void obtenirPosition()  {
        //on démarre le cercle de chargement
        setProgressBarIndeterminateVisibility(true);

        //On demande au service de localisation de nous notifier tout changement de position
        //sur la source (le provider) choisie, toute les minutes (60000millisecondes).
        //Le paramètre this spécifie que notre classe implémente LocationListener et recevra

       //les notifications.


        lManager.requestLocationUpdates(choix_source, 10000, 0, this);




    }
    private void stopper()  {
        lManager.removeUpdates(this);

    }
    class Task implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i <= 10; i++) {
                final int value = i;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bar.setProgress(value);

            }
            for (int i = 10; i >= 0; i--) {
                final int value = i;
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bar.setProgress(value);

            }
        }


    }
    //avant priver
    /*public void obtenirPosition2()  {
        //on démarre le cercle de chargement
        setProgressBarIndeterminateVisibility(true);

        //On demande au service de localisation de nous notifier tout changement de position
        //sur la source (le provider) choisie, toute les minutes (60000millisecondes).
        //Le paramètre this spécifie que notre classe implémente LocationListener et recevra

        //les notifications.




        lManager.requestLocationUpdates(choix_source, 5000, 0, this);
        ((TextView)findViewById(R.id.latitude)).setText(String.valueOf(location.getLatitude()));
        ((TextView)findViewById(R.id.longitude)).setText(String.valueOf(location.getLongitude()));
        ((TextView)findViewById(R.id.altitude)).setText(String.valueOf(location.getAltitude()));
        /*Geocoder geo = new Geocoder(MainActivity.this);
        try {
            //Ici on récupère la premiere adresse trouvé gràce à la position que l'on a récupéré
            List
                    <Address> adresses = geo.getFromLocation(location.getLatitude(),location.getLongitude(),1);

            if(adresses != null && adresses.size() == 1){
                Address adresse = adresses.get(0);
                //Si le geocoder a trouver une adresse, alors on l'affiche
                ((TextView)findViewById(R.id.adresse)).setText(String.format("%s, %s %s",
                        adresse.getAddressLine(0),
                        adresse.getPostalCode(),
                        adresse.getLocality()));
            }
            else {
                //sinon on affiche un message d'erreur
                ((TextView)findViewById(R.id.adresse)).setText("L'adresse n'a pu être déterminée");
            }
        } catch (IOException e) {
            e.printStackTrace();
            ((TextView)findViewById(R.id.adresse)).setText("L'adresse n'a pu être déterminée");
        }*/
     /*   posgpsBDD posgpsBDD= new posgpsBDD(this);

        TextView position = (TextView) findViewById(R.id.position);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

// Get the date today using Calendar object.
        Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
        String reportDate = df.format(today);
        TextView first = (EditText) findViewById(R.id.latitude);
        TextView second = (EditText) findViewById(R.id.longitude);
        String s1 = first.getText().toString();
        String s2 = second.getText().toString();
        posgps posgps=new posgps(s1,s2,reportDate);//afficherLocation2()

        //posgps posgps=new posgps(afficherLocation2(),reportDate);//champs 1 locations
        posgpsBDD.open();
        //bdd = maBaseSQLite.getWritableDatabase();
        posgpsBDD.insertposgps(posgps);
        //posgps posgpsfromBDD= posgpsBDD.getposgpsWithdate(posgps.getDate());
        //posgps posgpsfromBDD = posgpsBDD.getposgpsWithid(posgps.getId());
        //if(posgpsfromBDD != null){
        //  Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

        // ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));

        //}
        // String positions = db.showAll();
        //Toast.makeText(this,positions.toString(),Toast.LENGTH_LONG).show();
      /*  Toast.makeText(this,posgps.toString(),Toast.LENGTH_LONG).show();

        ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgps));
        //((TextView)findViewById(R.id.position)).setText(String.valueOf(positions));
        posgpsBDD.close();
    }*/




    private void  obtenirPosition2 () {

        //on démarre le cercle de chargement


        //On demande au service de localisation de nous notifier tout changement de position
        //sur la source (le provider) choisie, toute les minutes (60000millisecondes).
        //Le paramètre this spécifie que notre classe implémente LocationListener et recevra

        //les notifications
       // this.location = location;
       // Location location = lManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
       /* double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        ((TextView)findViewById(R.id.latitude)).setText(String.valueOf(longitude));
        ((TextView)findViewById(R.id.longitude)).setText(String.valueOf(latitude));
        ((TextView)findViewById(R.id.altitude)).setText(String.valueOf(location.getAltitude()));
*/
        int i=0;
for (i=0;i<=10;i++) {


    lManager.requestLocationUpdates(choix_source, 5000, 0, this);
    posgpsBDD posgpsBDD = new posgpsBDD(this);

    TextView position = (TextView) findViewById(R.id.position);

    DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss");


// Get the date today using Calendar object.
    Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
    String reportDate = df.format(today);
    TextView first = (EditText) findViewById(R.id.latitude);
    TextView second = (EditText) findViewById(R.id.longitude);
   String s1 = first.getText().toString();
    String s2 = second.getText().toString();

    //float s3=(float)location.getLatitude();
    //float s4=(float)location.getLongitude();

    posgps posgps = new posgps(s1, s2, reportDate,0,0);//afficherLocation2()

    //posgps posgps=new posgps(afficherLocation2(),reportDate);//champs 1 locations
    posgpsBDD.open();
    //bdd = maBaseSQLite.getWritableDatabase();
    posgpsBDD.insertposgps(posgps);
    //posgps posgpsfromBDD= posgpsBDD.getposgpsWithdate(posgps.getDate());
    //posgps posgpsfromBDD = posgpsBDD.getposgpsWithid(posgps.getId());
    //if(posgpsfromBDD != null){
    //  Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

    // ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));

    //}
    // String positions = db.showAll();
    //Toast.makeText(this,positions.toString(),Toast.LENGTH_LONG).show();
    //  Toast.makeText(this,posgps.toString(),Toast.LENGTH_LONG).show();

    ((TextView) findViewById(R.id.position)).setText(String.valueOf(posgps));
    //((TextView)findViewById(R.id.position)).setText(String.valueOf(positions));
    posgpsBDD.close();

}
            }








    public void afficherLocation() {

        //On affiche les informations de la position a l'écran
      ((TextView)findViewById(R.id.latitude)).setText(String.valueOf(location.getLatitude()));
      ((TextView)findViewById(R.id.longitude)).setText(String.valueOf(location.getLongitude()));
       //String s1=  String.valueOf(location.getLatitude());
        //String s2= String.valueOf(location.getLongitude());
        ((TextView)findViewById(R.id.altitude)).setText(String.valueOf(location.getAltitude()));
        //((TextView)findViewById(R.id.position)).setText(String.valueOf(location.getLatitude()));
        //((TextView)findViewById(R.id.position)).setText(String.valueOf(location.getLongitude()));
        //((TextView)findViewById(R.id.position)).setText(String.valueOf(location.getAltitude()));


    }
   public String  afficherLocation2() {
        //On affiche les informations de la position a l'écran

        TextView first = (TextView) findViewById(R.id.position);
        String s1 = first.getText().toString();
        TextView second = (EditText) findViewById(R.id.longitude);
        String s2 = second.getText().toString();
        String S3= s1+ s2;

        return s1;



       //((TextView)findViewById(R.id.position)).setText(String.valueOf(S3));
      // ((TextView)findViewById(R.id.position)).setText(String.valueOf(location.getLongitude()));
        //((TextView)findViewById(R.id.altitude)).setText(String.valueOf(location.getAltitude()));
    }
    private void afficherAdresse() {
        setProgressBarIndeterminateVisibility(true);
        //String v1="CREATE TABLE table_GPS (ID INTEGER PRIMARY KEY AUTOINCREMENT, posact TEXT NOT NULL, date TEXT NOT NULL);";
        //bdd.execSQL(v1);

        //Le geocoder permet de récupérer ou chercher des adresses
        //gràce à un mot clé ou une position
      /*  Geocoder geo = new Geocoder(MainActivity.this);
        try {
            //Ici on récupère la premiere adresse trouvé gràce à la position que l'on a récupéré
            List
                    <Address> adresses = geo.getFromLocation(location.getLatitude(),location.getLongitude(),1);

            if(adresses != null && adresses.size() == 1){
                Address adresse = adresses.get(0);
                //Si le geocoder a trouver une adresse, alors on l'affiche
                ((TextView)findViewById(R.id.adresse)).setText(String.format("%s, %s %s",
                        adresse.getAddressLine(0),
                        adresse.getPostalCode(),
                        adresse.getLocality()));
            }
            else {
                //sinon on affiche un message d'erreur
                ((TextView)findViewById(R.id.adresse)).setText("L'adresse n'a pu être déterminée");
            }
        } catch (IOException e) {
            e.printStackTrace();
            ((TextView)findViewById(R.id.adresse)).setText("L'adresse n'a pu être déterminée");
        }
        //on stop le cercle de chargement
        setProgressBarIndeterminateVisibility(false);
        */
        posgpsBDD posgpsBDD= new posgpsBDD(this);

        TextView position = (TextView) findViewById(R.id.position);

        DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss");

// Get the date today using Calendar object.
        Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
        String reportDate = df.format(today);
        //TextView second = (EditText) findViewById(R.id.longitude);
        //String s2 = second.getText().toString();
        String s1=String.valueOf(location.getLatitude());
        String s2=String.valueOf(location.getLongitude());
        //posgps posgps=new posgps(input.getText().toString(),s2,reportDate,0,0);//afficherLocation2()
        posgps posgps=new posgps(s1,s2,reportDate,0,0);

        //posgps posgps=new posgps(afficherLocation2(),reportDate);//champs 1 locations
        posgpsBDD.open();
        //bdd = maBaseSQLite.getWritableDatabase();
        posgpsBDD.insertposgps(posgps);
        //posgps posgpsfromBDD= posgpsBDD.getposgpsWithdate(posgps.getDate());
        //posgps posgpsfromBDD = posgpsBDD.getposgpsWithid(posgps.getId());
        //if(posgpsfromBDD != null){
         //  Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

           // ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));

        //}
      // String positions = db.showAll();
        //Toast.makeText(this,positions.toString(),Toast.LENGTH_LONG).show();
       Toast.makeText(this,posgps.toString(),Toast.LENGTH_LONG).show();

       //temporaire ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgps));
        //((TextView)findViewById(R.id.position)).setText(String.valueOf(positions));
        posgpsBDD.close();
    }
    private void nextrec(){
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        //TextView position = (TextView) findViewById(R.id.position);
        //DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

// Get the date today using Calendar object.
        //Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
        //String reportDate = df.format(today);
        //posgps posgps=new posgps(input.getText().toString(),reportDate);//afficherLocation2()

       // posgps posgps=new posgps(afficherLocation2(),reportDate);//champs 1 locations
        posgpsBDD.open();
        //bdd = maBaseSQLite.getWritableDatabase();
        //posgpsBDD.insertposgps(posgps);
        //posgps posgpsfromBDD= posgpsBDD.getposgpsWithdate(posgps.getDate());

        posgps posgpsfromBDD = posgpsBDD.getposgpsWithidN(2);
       // posgps posgpsfromBDD = posgpsBDD.showAll();
        //if(posgpsfromBDD != null){
        //   Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

        // ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));

        //}
        Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

        ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));
        posgpsBDD.close();
    }
    private void firstrec(){
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        //TextView position = (TextView) findViewById(R.id.position);
        //DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

// Get the date today using Calendar object.
        //Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
        //String reportDate = df.format(today);
        //posgps posgps=new posgps(input.getText().toString(),reportDate);//afficherLocation2()

        // posgps posgps=new posgps(afficherLocation2(),reportDate);//champs 1 locations
        posgpsBDD.open();
        //bdd = maBaseSQLite.getWritableDatabase();
        //posgpsBDD.insertposgps(posgps);
        //posgps posgpsfromBDD= posgpsBDD.getposgpsWithdate(posgps.getDate());

      posgps posgpsfromBDD = posgpsBDD.getposgpsWithidF(1); //attention a remettre a ok !!!!!
        //posgps posgpsfromBDD =posgpsBDD.showAll();
        //if(posgpsfromBDD != null){
        //   Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

        // ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));

        //}
       // String positions = posgpsBDD.showAll();
        //Toast.makeText(this,positions.toString(),Toast.LENGTH_LONG).show();
       Toast.makeText(this,posgpsfromBDD.toString(),Toast.LENGTH_LONG).show();

        ((TextView)findViewById(R.id.position)).setText(String.valueOf(posgpsfromBDD));
        posgpsBDD.close();
    }
    private void supprimerElement() {
        posgpsBDD posgpsBDD= new posgpsBDD(this);
       posgpsBDD.open();
        posgps posgpsfromBDD = posgpsBDD.getposgpsWithid(2);
        if(posgpsfromBDD != null){
            //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour
            posgpsBDD.removeposgpsWithID(posgpsfromBDD.getId());
            Toast.makeText(this,posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();
            //on supprime le livre de la BDD grâce à son ID

        }


    }
    private Float testafficghage() {
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        posgpsBDD.open();
        float lat = posgpsBDD.obtenirelement(2);

            //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour

           // Toast.makeText(this,posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();
            return lat;
            //on supprime le livre de la BDD grâce à son ID



    }
    private Float testafficghage2() {
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        posgpsBDD.open();
        float lng = posgpsBDD.obtenirelement2(2);

        //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour

        // Toast.makeText(this,posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();
        return lng;
        //on supprime le livre de la BDD grâce à son ID




    }
    private Float testafficghage3() {
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        posgpsBDD.open();
        float lng = posgpsBDD.obtenirelement3(2);

        //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour

        // Toast.makeText(this,posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();
        return lng;
        //on supprime le livre de la BDD grâce à son ID




    }
    private Float testafficghage4() {
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        posgpsBDD.open();
        float lng = posgpsBDD.obtenirelement4(2);

        //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour

        // Toast.makeText(this,posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();
        return lng;
        //on supprime le livre de la BDD grâce à son ID




    }
    private String testafficghage5() {
        posgpsBDD posgpsBDD= new posgpsBDD(this);
        posgpsBDD.open();
        String lng = posgpsBDD.obtenirelement5(2);

        //On affiche les nouvelle info du livre pour vérifié que le titre du livre a bien été mis à jour

        // Toast.makeText(this,posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();
        return lng;
        //on supprime le livre de la BDD grâce à son ID




    }
    public void ouvriractivity2(){
        startActivity(new Intent(MainActivity.this, MainActivity2Activity.class));
    }
    public void ouvrirchart(){
        startActivity(new Intent(MainActivity.this, MainActivity3Activity.class));

    }
   public void onLocationChanged(Location location) {
        //Lorsque la position change...
        Log.i("Tuto géolocalisation", "La position a changé.");
        //... on stop le cercle de chargement
        setProgressBarIndeterminateVisibility(false);
        //... on active le bouton pour afficher l'adresse
       findViewById(R.id.afficherAdresse).setEnabled(true);
        //... on sauvegarde la position
        this.location = location;


     //  TextView v = (TextView)findViewById(R.id.adresse);
      // v.setText("Current speed:"+ String.valueOf(location.getSpeed()));

       //... on l'affiche
       afficherLocation();
      afficherAdresse();
       distance();




       //  testafficghage();
      // distance();

        //... et on spécifie au service que l'on ne souhaite plus avoir de mise à jour
        //lManager.removeUpdates(this);
    }
   private void distance() {
       /* String selectQuery = "SELECT COL_positionsactuelles FROM table_GPS WHERE COL_positionsactuelles =1 ";
        Cursor c = posgpsBDD.rawQuery(selectQuery, new String[] { fileName });
        if (c.moveToFirst()) {
            temp_address = c.getString(0);
        }*/ //c.moveToFirst();
       //c.moveToLast();
       //On créé un livre
     //temporoaire  posgps posgps = new posgps();
       //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor

       //posgps.setPosact_lat(c.getString(NUM_COL_positionsactuelles));
       // temporaireposgpsBDD posgpsBDD = new posgpsBDD(this);

      //temporaire posgpsBDD.open();

      //temporaire posgps posgpsfromBDD = posgpsBDD.getposgpsWithidF(2);
       // posgps c = posgpsBDD.getposgpsWithidF(2);

       float pk = (float) (180 / 3.14169);
       //float lat_a=(float) location.getLatitude();

       //float lat_a = (float) location.getLatitude();
       //float lng_a = (float) location.getLongitude();
       float lat_a =  testafficghage3();
       float lng_a = testafficghage4();
       float lat_b =  testafficghage();
      // Random randomGenerator = new Random();
      // int randomInt = randomGenerator.nextInt(50);
       //float lat_b = randomInt;

       //float lng_b = 4.529406f;
       float lng_b =testafficghage2();
       String date=testafficghage5();
       //float lat_a= (float) 1,2567;

       float a1 = lat_a / pk;
       float a2 = lng_a / pk;
       float b1 = lat_b / pk;
       float b2 = lng_b / pk;

       float t1 = FloatMath.cos(a1) * FloatMath.cos(a2) * FloatMath.cos(b1) * FloatMath.cos(b2);
       float t2 = FloatMath.cos(a1) * FloatMath.sin(a2) * FloatMath.cos(b1) * FloatMath.sin(b2);
       float t3 = FloatMath.sin(a1) * FloatMath.sin(b1);
       double tt = Math.acos(t1 + t2 + t3);
       double tt2 = 6371000 * tt;

       //attention a remettre a ok !!!!!

       //tempo Toast.makeText(this, posgpsfromBDD.toString(), Toast.LENGTH_LONG).show();

       //((TextView) findViewById(R.id.position)).setText(String.valueOf(tt2));
//tempo       posgpsBDD.close();


       ((TextView) findViewById(R.id.adresse)).setText(String.valueOf(tt2));
       Log.d("ADebugTag", "Value: " + Float.toString(lat_b));
       Log.d("ADebugTag", "Value: " + Float.toString(lng_b));
       Log.d("ADebugTag", "Value: " + Float.toString(lat_a));
       Log.d("ADebugTag", "Value: " + Float.toString(lng_a));
       ((TextView) findViewById(R.id.position)).setText("valeur bdd"+"\n"+String.valueOf(lat_b)+ "\n" + String.valueOf(lng_b)
                                                        +"\n"+ "valeur now"+"\n"+String.valueOf(lat_a)+ "\n" + String.valueOf(lng_a));
       //Toast.makeText(this,Float.toString(lat_b), Toast.LENGTH_LONG).show();
       //Toast.makeText(this,Float.toString(lng_b), Toast.LENGTH_LONG).show();
       //Toast.makeText(this,Float.toString(lat_a), Toast.LENGTH_LONG).show();
       //Toast.makeText(this,Float.toString(lng_a), Toast.LENGTH_LONG).show();
   }
    public void createPDF()
    {
        Document doc = new Document();


        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/droidText";

            File dir = new File(path);
            if(!dir.exists())
                dir.mkdirs();

            Log.d("PDFCreator", "PDF Path: " + path);


            File file = new File(dir, "sample.pdf");
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();


            Paragraph p1 = new Paragraph("Hi! I am generating my first PDF using DroidText");
            Font font= FontFactory.getFont("Times-Roman");
           // Font paraFont= new Font(Font.COURIER);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(font);

            //add paragraph to document
            doc.add(p1);

            Paragraph p2 = new Paragraph("This is an example of a simple paragraph");
            //Font paraFont2= new Font(Font.,14.0f,Color.GREEN);
            Font font2= FontFactory.getFont("Times-Roman");
            p2.setAlignment(Paragraph.ALIGN_CENTER);
            p2.setFont(font2);

            doc.add(p2);

           /* ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.logo1);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
            Image myImg = Image.getInstance(stream.toByteArray());
            myImg.setAlignment(Image.MIDDLE);

            //add image to document
            doc.add(myImg);*/

            //set footer
            /*Phrase footerText = new Phrase("This is an example of a footer");
            HeaderFooter pdfFooter = new HeaderFooter(footerText, false);
            doc.setFooter(pdfFooter);*/



        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally
        {
            doc.close();
        }

    }



    public void onProviderDisabled(String provider) {
        //Lorsque la source (GSP ou réseau GSM) est désactivé
        Log.i("Tuto géolocalisation", "La source a été désactivé");
        //...on affiche un Toast pour le signaler à l'utilisateur
        Toast.makeText(MainActivity.this,
                String.format("La source \"%s\" a été désactivé", provider),
                Toast.LENGTH_SHORT).show();
        //... et on spécifie au service que l'on ne souhaite plus avoir de mise à jour
        lManager.removeUpdates(this);
        //... on stop le cercle de chargement
        setProgressBarIndeterminateVisibility(false);
    }

    public void onProviderEnabled(String provider) {
        Log.i("Tuto géolocalisation", "La source a été activé.");
    }
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("Tuto géolocalisation", "Le statut de la source a changé.");
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
