package com.example.dav.myapplication_gps;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.FloatMath;
import android.util.Log;
import android.widget.TextView;

import java.net.IDN;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;


/**
 * Created by Dav on 27/02/2015.
 */
public class posgpsBDD {
    private static final int VERSION_BDD = 29;
    private static final String NOM_BDD = "gps.db";
    //Table GPS
    private static final String TABLE_GPS = "table_GPS";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_positionsactuelles = "posact_lat";
    private static final int NUM_COL_positionsactuelles = 1;
    private static final String COL_positionsactuelles2 = "posact_long";
    private static final int NUM_COL_positionsactuelles2 = 2;
    private static final String COL_date = "date";
    private static final int NUM_COL_date = 3;
    private static final String COL_positionsactuellesf = "posact_latf";
    private static final int NUM_COL_positionsactuellesf = 4;
    private static final String COL_positionsactuelles2f = "posact_longf";
    private static final int NUM_COL_positionsactuelles2f = 5;
    //rajout position 2 07-04
    private String[] allColumns = { COL_ID,
            COL_positionsactuelles,COL_positionsactuelles2, COL_date };

    //table Utilisateur
    private static final String TABLE_USER = "table_USER";
    private static final String COL_ID2 = "ID2";
    private static final int NUM_COL_ID2 = 0;
    private static final String COL_gender = "gender";
    private static final int NUM_COL_gender= 1;
    private static final String COL_name = "name";
    private static final int NUM_COL_name = 2;
    private static final String COL_firstname = "firstname";
    private static final int NUM_COL_firstname = 3;
    private static final String COL_age = "age";
    private static final int NUM_COL_age = 4;
    private static final String COL_weight = "weight";
    private static final int NUM_COL_weight = 5;
    private static final String COL_taille = "taille";
    private static final int NUM_COL_taille = 6;





    private SQLiteDatabase bdd;

    private MaBaseSQLite maBaseSQLite;

    public posgpsBDD(Context context){
        //On créer la BDD et sa table

        maBaseSQLite = new MaBaseSQLite(context, NOM_BDD,null,VERSION_BDD);//, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase  getBdd (){
        return bdd;
    }

    public long insertposgps(posgps posgps){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_positionsactuelles, posgps.getPosact_lat());
        values.put(COL_positionsactuelles2, posgps.getPosact_long());
        values.put(COL_positionsactuellesf, posgps.getPosact_latf());
        values.put(COL_positionsactuelles2f, posgps.getPosact_longf());
        values.put(COL_date, posgps.getDate());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_GPS, null, values);
    }
   /* public long insertposgpstest(){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_positionsactuelles, 50.45);
        values.put(COL_positionsactuelles2, 3.56);
        values.put(COL_positionsactuellesf, 0);
        values.put(COL_positionsactuelles2f, 0);
        values.put(COL_date, 0);
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_GPS, null, values);
    }*/
    public long insertusergps(usergps usergps){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        //values.put(COL_gender, usergps.getGender());
        values.put(COL_name, usergps.getName());
        values.put(COL_firstname, usergps.getFirstname());
        values.put(COL_age, usergps.getAge());
        values.put(COL_weight, usergps.getWeight());
        values.put(COL_taille, usergps.getTaille());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_USER, null, values);
    }

    public int updateposgps(int id, posgps posgps){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simple préciser quelle livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_positionsactuelles, posgps.getPosact_lat());
        values.put(COL_positionsactuelles2, posgps.getPosact_long());
        values.put(COL_positionsactuellesf, posgps.getPosact_latf());
        values.put(COL_positionsactuelles2f, posgps.getPosact_longf());
        values.put(COL_date, posgps.getDate());
        return bdd.update(TABLE_GPS, values, COL_ID + " = " +id, null);
    }

    public int removeposgpsWithID(int id){
        //Suppression d'un livre de la BDD grâce à l'ID
        return bdd.delete(TABLE_GPS, COL_ID + " = " +id, null);
    }

    public posgps getposgpsWithdate(String date){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date}, COL_date /*+ " LIKE \"" + date +"\""*/, null, null, null, null,null);
        return cursorToposgps(c);
    }
//essai sur colone position
    public posgps getposgpsWithid(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2 ,COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        return cursorToposgps(c1);
    }
    public posgps getposgpsWithidN(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2,COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        return cursorToposgpsN(c1);
    }

    public posgps getposgpsWithidF(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        //Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
       // Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_positionsactuelles}/*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);

        return cursorToposgpsF(c1);
    }
    public Float obtenirelement(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        //Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        // Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_positionsactuelles}/*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);

        return cursortest(c1);
    }
    public Float obtenirelement2(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        //Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        // Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_positionsactuelles}/*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);

        return cursortest2(c1);
    }
    public Float obtenirelement3(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        //Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        // Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_positionsactuelles}/*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);

        return cursortest3(c1);
    }
    public Float obtenirelement4(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        //Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        // Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_positionsactuelles}/*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);

        return cursortest4(c1);
    }
    public String obtenirelement5(int id){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        //Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*///,// null, null, null, null,null);
        Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_ID, COL_positionsactuelles, COL_positionsactuelles2, COL_date,COL_positionsactuellesf,COL_positionsactuelles2f}, COL_ID /*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);
        // Cursor c1 = bdd.query(TABLE_GPS, new String[] {COL_positionsactuelles}/*+ " LIKE \"" + ID +"\""*/, null, null, null, null,null);

       return cursortest5(c1);
    }


    //Cette méthode permet de convertir un cursor en un livre
    private posgps cursorToposgps(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //c.moveToLast();
        //On créé un livre
        posgps posgps = new posgps();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        posgps.setId(c.getInt(NUM_COL_ID));
        posgps.setPosact_lat(c.getString(NUM_COL_positionsactuelles));
        posgps.setPosact_long(c.getString(NUM_COL_positionsactuelles2));
        posgps.setDate(c.getString(NUM_COL_date));
        posgps.setPosact_latf(c.getFloat(NUM_COL_positionsactuellesf));
        posgps.setPosact_longf(c.getFloat(NUM_COL_positionsactuelles2f));
        //On ferme le cursor
        c.close();

        //On retourne le livre
        return posgps;
    }
    private posgps cursorToposgpsN(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
       // if (c.getCount() == 0)
         //   return null;

        //Sinon on se place sur le premier élément
       // c.moveToLast();
        c.moveToNext();
        c.moveToNext();
        c.moveToNext();
        //On créé un livre
        posgps posgps = new posgps();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        posgps.setId(c.getInt(NUM_COL_ID));
        posgps.setPosact_lat(c.getString(NUM_COL_positionsactuelles));
        posgps.setPosact_long(c.getString(NUM_COL_positionsactuelles2));
        posgps.setDate(c.getString(NUM_COL_date));
        //On ferme le cursor
        c.close();

        //On retourne le livre
        return posgps;
    }
    //test sur un resultat de curseur
   /* private posgps   cursorToposgpsTest(){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        // if (c.getCount() == 0)
        //   return null;
        Cursor c1= bdd.query(TABLE_GPS,allColumns,null,null,null,null,null);
        int iPos1= c1.getColumnIndex(COL_positionsactuelles);
        int iPos2=c1.getColumnIndex(COL_positionsactuelles2);
        int date= c1.getColumnIndex(COL_date);
        String result="";

        for (c1.moveToFirst(); ! c1.isAfterLast(); c1.moveToNext())       {

           result=result + c1.getString(iPos1) + ""+ c1.getString(iPos2)+ "" + c1.getString(date)+"\n";

        }

        //Sinon on se place sur le premier élément
        // c.moveToLast();


        //On retourne le livre
        return posgps;
    }*/
    private posgps cursorToposgpsF(Cursor c) {
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        // if (c.getCount() == 0)
        //   return null;


        //Sinon on se place sur le premier élément
     //  c.moveToFirst();
        c.moveToLast();
        //c.moveToNext();
        //On créé un livre
        posgps posgps = new posgps();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        // posgps.setId(c.getInt(NUM_COL_ID));
        posgps.setPosact_lat(c.getString(NUM_COL_positionsactuelles));
        posgps.setPosact_long(c.getString(NUM_COL_positionsactuelles2));
        posgps.setDate(c.getString(NUM_COL_date));

        //On ferme le cursor
        c.close();


        //On retourne le livre
        return posgps;
    }
    public float cursortest (Cursor c) {
if (c.getCount()==1) {
    c.moveToFirst();
}else{
c.moveToLast();
c.moveToPrevious();}

        String posact_lat;


        posact_lat = (c.getString(NUM_COL_positionsactuelles));
        float f = Float.parseFloat(posact_lat);
        c.close();
        return f;
    }
    public float cursortest3 (Cursor c) {

            c.moveToFirst();



        String posact_lat;


        posact_lat = (c.getString(NUM_COL_positionsactuelles));
        float f = Float.parseFloat(posact_lat);
        c.close();
        return f;
    }




    public float cursortest2 (Cursor c){

        if (c.getCount()==1) {
            c.moveToFirst();
        }else{
            c.moveToLast();
            c.moveToPrevious();}
        String posact_long ;


        posact_long=(c.getString(NUM_COL_positionsactuelles2));
        float f = Float.parseFloat(posact_long);
        c.close();


        return f;

    }
    public float cursortest4 (Cursor c) {

        c.moveToFirst();



        String posact_long;


        posact_long = (c.getString(NUM_COL_positionsactuelles2));
        float f = Float.parseFloat(posact_long);
        c.close();
        return f;
    }
   public String cursortest5 (Cursor c) {
        if (c.getCount()==1) {
            c.moveToFirst();
        }else{
            c.moveToLast();
            c.moveToPrevious();}

        String date;


        date = (c.getString(NUM_COL_date));

       SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss");

      //Date date1=sdf.parse(date);


        return date;
    }

   /* a reactiver public String showAll() {

        List<posgps> foods = new LinkedList<posgps>();
        //int t1=38;
        //String query = "SELECT * FROM  +"TABLE_GPS+ " where `" + COL_ID + "'=" + 't1';
        String query = "SELECT * FROM TABLE_GPS";
        Cursor cursor = bdd.rawQuery(query, null);
        open();
       // Cursor cursor = bdd.rawQuery( "SELECT * FROM  TABLE_GPS where COL_ID"+64+"'", null);

        posgps food = null;

        if (cursor.moveToFirst()) {
            do {
                food = new posgps();
                food.setId(cursor.getInt(NUM_COL_ID));
                food.setPosact_lat(cursor.getString(NUM_COL_positionsactuelles));
                food.setPosact_long(cursor.getString(NUM_COL_positionsactuelles2));
                food.setDate(cursor.getString(NUM_COL_date));

                foods.add(food);
            } while (cursor.moveToNext());
            cursor.close();
        }

      //  Log.d("SQLite DB : Show All : ", foods.toString());
        return food.toString();

    }*/
   public String showAll() {





       List<posgps> foods = new ArrayList<posgps>();
       //int t1=38;
       //String query = "SELECT * FROM  +"TABLE_GPS+ " where `" + COL_ID + "'=" + 't1';
       String query = "SELECT * FROM TABLE_GPS";
       Cursor cursor = bdd.rawQuery(query, null);
       open();
       // Cursor cursor = bdd.rawQuery( "SELECT * FROM  TABLE_GPS where COL_ID"+64+"'", null);

       posgps food = null;

       if (cursor.moveToFirst()) {
           do {
               food = new posgps();

               food.setPosact_lat(cursor.getString(NUM_COL_positionsactuelles));



               foods.add(food);
           } while (cursor.moveToNext());
           cursor.close();
       }

       //  Log.d("SQLite DB : Show All : ", foods.toString());

      return food.toString();
   }

   /* public double distance() {
        String selectQuery = "SELECT COL_positionsactuelles FROM table_GPS WHERE COL_positionsactuelles =1 ";
        Cursor c = bdd.rawQuery(selectQuery, null);
        posgps food = null;
        if (c.moveToFirst()) {
            food = new posgps();
            food.setPosact_lat(c.getString(1));

        }
        float pk = (float) (180 / 3.14169);
        //float lat_a=(float) location.getLatitude();
        float lat_a=(float) food;
        float lng_a=5;
        float lat_b=(float) location.getLongitude();
        float lng_b=8;
        //float lat_a= (float) 1,2567;

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        float t1 = FloatMath.cos(a1) * FloatMath.cos(a2) * FloatMath.cos(b1) * FloatMath.cos(b2);
        float t2 = FloatMath.cos(a1) * FloatMath.sin(a2) * FloatMath.cos(b1) * FloatMath.sin(b2);
        float t3 = FloatMath.sin(a1) * FloatMath.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);
        double tt2= 6371 * tt;
        return  tt2;


        //((TextView)findViewById(R.id.textView2)).setText(String.valueOf(tt2));

    }*/
    /*public List<posgps> getAllContacts() {
        List<posgps> contactList = new ArrayList<posgps>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + table_gps;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                posgps posgps = new posgps();
                posgps.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }*

        // return contact list
        return contactList;
    }*/
    //tester pour aller dun record a lautre
   /* private posgps cursorToposgpsN(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //c.moveToLast();
        //On créé un livre
        while(!c.isAfterLast()) {

            c.getString(c.getColumnIndex("NUM_COL_positionsactuelles"));
            c.getString(c.getColumnIndex("NUM_COL_ID"));
            c.getString(c.getColumnIndex("NUM_COL_date"));

            c.moveToNext();
        }
        //On ferme le cursor
        c.close();

        //On retourne le livre
        return posgps;
    }*/
   /* public posgps createComment(String posgps) {
        ContentValues values = new ContentValues();
        values.put(COL_positionsactuelles, posgps);
        long insertId = bdd.insert(TABLE_GPS, null,
                values);
        Cursor cursor = bdd.query(TABLE_GPS,
                allColumns,COL_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        posgps newComment = cursorToposgps(cursor);
        cursor.close();
        return newComment;
    }
    public List<posgps> getAllComments() {
        List<posgps> comments = new ArrayList<posgps>();

        Cursor cursor = bdd.query(TABLE_GPS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            posgps posgps = cursorToComment(cursor);
            comments.add(posgps);
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return comments;
    }
    private posgps cursorToComment(Cursor cursor) {
        posgps posgps = new posgps();
        posgps.setId(cursor.getInt(0));
        posgps.setPosact_lat(cursor.getString(1));
        posgps.setPosact_long(cursor.getString());
        return posgps;
    }*/
}
