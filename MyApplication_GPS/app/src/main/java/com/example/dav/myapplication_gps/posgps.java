package com.example.dav.myapplication_gps;

import android.text.format.Time;

/**
 * Created by Dav on 27/02/2015.
 */
public class posgps {
    private int id;
    private String posact_lat;
    private String posact_long;
    private String date;
    private float posact_latf;
    private float posact_longf;

    public posgps(){}

    public posgps(String posact_lat, String posact_long, String date, float posact_latf, float posact_longf){
        this.posact_lat = posact_lat;
        this.posact_long = posact_long;
        this.date = date;
        this.posact_latf = posact_latf;
        this.posact_longf = posact_longf;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPosact_lat() {
        return posact_lat;
    }
    public float getPosact_latf() {
        return posact_latf;
    }

    public void setPosact_lat(String posact_lat) {
        this.posact_lat = posact_lat;
    }
    public void setPosact_latf(float posact_latf) {
        this.posact_latf = posact_latf;
    }

    public String getPosact_long() {
        return posact_long;
    }
    public float getPosact_longf() {
        return posact_longf;
    }
    public void setPosact_long(String posact_long) {
        this.posact_long = posact_long;
    }

    public void setPosact_longf(float posact_longf) {
        this.posact_longf = posact_longf;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toString(){
        return +id+"\n"+posact_lat+"\n"+posact_long+"\n"+date;
    }
    public float tofloat(){
        return posact_latf;
    }

   /* public String toString(){
        return "ID : "+id+"\npositions actuelles : "+posact_lat+"\npositions actuelles2 : "+posact_long+"\ndate : "+date;
    }*/
}
