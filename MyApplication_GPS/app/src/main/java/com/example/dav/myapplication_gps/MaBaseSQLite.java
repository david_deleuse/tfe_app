package com.example.dav.myapplication_gps;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by Dav on 27/02/2015.
 */

    public class MaBaseSQLite extends SQLiteOpenHelper {
        private static final String TABLE_GPS = "table_GPS";
        private static final String COL_ID = "ID";
        private static final String COL_positionsactuelles = "posact_lat";
        private static final String COL_positionsactuelles2 = "posact_long";
        private static final String COL_DATE = "date";
        private static final String COL_positionsactuellesf = "posact_latf";
        private static final String COL_positionsactuelles2f = "posact_longf";

    private static final String TABLE_USER = "table_USER";
    private static final String COL_ID2 = "ID2";
    private static final String COL_gender = "gender";
    private static final String COL_name = "name";
    private static final String COL_firstname = "firstname";
    private static final String COL_age = "age";
    private static final String COL_weight = "weight";
    private static final String COL_taille = "taille";


    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_GPS + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_positionsactuelles+ " TEXT NOT NULL, "
            + COL_positionsactuelles2+ " TEXT NOT NULL, "+ COL_DATE + " TEXT NOT NULL,"+COL_positionsactuellesf+ " REAL, "
            + COL_positionsactuelles2f+ " REAL );";

    private static final String CREATE_BDD2 ="CREATE TABLE "+ TABLE_USER + " ("
            + COL_ID2 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_name+ " TEXT NOT NULL, "
            + COL_firstname+ " TEXT NOT NULL, "+ COL_age + " TEXT NOT NULL,"+COL_weight+ " TEXT NOT NULL, "
            + COL_taille+ " TEXT NOT NULL );";
    //rajouter + COL_gender+ " TEXT NOT NULL, "

        public MaBaseSQLite(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //on créé la table à partir de la requête écrite dans la variable CREATE_BDD
            db.execSQL(CREATE_BDD);
            db.execSQL(CREATE_BDD2);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //On peut fait ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
            //comme ça lorsque je change la version les id repartent de 0
            db.execSQL("DROP TABLE " + TABLE_GPS + ";");
            onCreate(db);
        }

}

